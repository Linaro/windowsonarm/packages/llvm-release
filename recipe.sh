#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

set -euo pipefail

checkout()
{
    if [ -d llvm-project ]; then return; fi

    git clone https://github.com/llvm/llvm-project --single-branch --branch $version --depth 1
    pushd llvm-project
    git log -n1
    popd
}

build()
{
    pushd llvm-project

    mkdir -p build
    pushd build

    if [ ! -f .configured ]; then
        cmake -G Ninja ../llvm \
            -DCMAKE_BUILD_TYPE=Release \
            -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;lld;flang;compiler-rt" \
            -DCMAKE_C_COMPILER=clang-cl.exe \
            -DCMAKE_CXX_COMPILER=clang-cl.exe \
            -DCPACK_SYSTEM_NAME=woa64
        touch .configured
    fi

    ninja
    cpack -G ZIP

    popd
    popd

    cp -f llvm-project/build/*woa64.zip $out_dir/
}

checkout
build
